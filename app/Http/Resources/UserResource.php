<?php

namespace App\Http\Resources;


use App\Http\Resources\ContactCollection;
use App\Http\Resources\NoteCollection;
use App\Http\Resources\ProjectCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Project;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'contacts' => new ContactCollection($this->contacts),
            'notes' => new NoteCollection($this->notes),
            'projects' => ProjectResource::collection($this->projects),
            'created_at' => (String)$this->created_at,
            'updated_at' => (String)$this->updated_at,
        ];
    }
}
