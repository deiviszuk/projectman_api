<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Auth;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::where('user_id','=',Auth::user()->id)->get();
        return $projects;
    }


    public function show(Project $project)
    {
        //dd($project);
        if ( $project->user_id === Auth::user()->id )
        {
        $project = Project::where('id','=',$project->id)->get();
        return $project;
        } else {
        return "Not Authorized User!!!";
        }
    }

    
    public function store(Request $request)
    {   

        $project = new Project;
        $project->user_id       =   Auth::user()->id;
        $project->name          =   $request->input('name');
        $project->description   =   $request->input('description');        
        $project->deadline      =   $request->input('deadline');        
        $project->save();
        
        return $project;
    }


    public function update(Request $request, Project $project)
    {
        $project = Project::where('id','=',$project->id)->first();
        $project->name          =   $request->input('name');
        $project->description   =   $request->input('description');
        $project->deadline      =   $request->input('deadline');  
        $project->save();

        return $project;
    }


    public function destroy(Project $project)
    {
        $project = Project::where('id','=',$project->id)->first();
        $project ->delete();
        
        return "Project Deleted !!!";
    }

/////////////////
/// Task Part ///
/////////////////

    public function indexTasks(Project $project, Request $request)
    {

        $tasks = Task::where('project_id','=',$project->id)->get();
        
        return $tasks;
    }

    public function showTask(Project $project, Request $request, Task $task)
    {
        $task = Task::where([
                ['project_id','=',$project->id],
                ['id', '=', $task->id]
            ])->get();
        
        return $task;
    }

    public function addTask(Project $project, Request $request)
    {

        $task = new Task;
        $task->project_id    =   $project->id;
        $task->description   =   $request->input('description');        
        $task->save();
        
        return $task;
    }


    public function updateTask(Project $project, Request $request, Task $task)
    {
        $task = Task::where([
            ['project_id','=',$project->id],
            ['id', '=', $task->id]
        ])->first();
        $task->project_id    =   $project->id;
        $task->description   =   $request->input('description');        
        $task->save();
        
        return $task;

    }

    public function finishTask(Project $project, Request $request, Task $task)
    {
        $task = Task::where([
            ['project_id','=',$project->id],
            ['id', '=', $task->id]
        ])->first();
        $task->project_id    =   $project->id;
        $task->finished      =   $request->input('finished');
        $task->save();
    }

    public function destroyTask(Project $project, Request $request, Task $task)
    {
        $task = Task::where([
            ['project_id','=',$project->id],
            ['id', '=', $task->id]
        ])->first();
        $task->delete();
        
        return "Task Deleted !!!";
    }
}
