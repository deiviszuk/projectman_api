<?php

namespace App\Http\Controllers;


use App\Note;
use Illuminate\Http\Request;
use Auth;

class NoteController extends Controller
{
    protected function denyAccessUnlessUserCanView(Note $note)
    {
        if ( $note->user_id !== Auth::user()->id ) 
            abort(403, 'Access denied');

    }

    public function index()
    {
        $notes = Note::where('user_id','=',Auth::user()->id)->get();
        return $notes;
    }


    public function show(Note $note)
    {
        $this->denyAccessUnlessUserCanView($note);

        $note = Note::where('id','=',$note->id)->get();
        return $note;
    }

    
    public function store(Request $request)
    {   

        $note = new Note;
        $note->user_id       =   Auth::user()->id;
        $note->name          =   $request->input('name');
        $note->description   =   $request->input('description');        
        $note->save();
        
        return $note;
    }
    
    public function update(Request $request, Note $note)
    {
        $note = Note::where('id','=',$note->id)->first();
        $note->name          =   $request->input('name');
        $note->description   =   $request->input('description');
        $note->save();

        return $note;
    }

    
    public function destroy(Note $note)
    {
        $note = Note::where('id','=',$note->id)->first();
        $note ->delete();
        
        return "Note Deleted !!!";
    }
}