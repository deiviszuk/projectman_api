<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
        
    
    public function index()
    {
        return new UserResource(Auth::user());
    }

   
    public function create()
    {
        //
    }

    
    public function store(Request $request){

        //You should add validation before creating the user.
     
        $user = User::create([
           'name'   => $request->name,
           'username'   => $request->username,
           'email'  => $request->email,
           'name'   => $request->name,
           'password' => bcrypt($request->password)
        ]);
     
        if(!$user){
           return response(["error" => "Klaida registracijos metu!"], 400);
        }
     
        return response(["user" => $user], 200);
     
     }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
