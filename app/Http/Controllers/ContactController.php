<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Auth;
use Response;


class ContactController extends Controller
{
    

   public function index() {
 
    $contacts = Contact::where('user_id','=',Auth::user()->id)->get();

    return array_values($contacts->toArray());
    }

    public function show(Contact $contact)
    {
        //dd( $contact->user_id === Auth::user()->id );
        if ( $contact->user_id === Auth::user()->id )
        {
        $contact = Contact::where('id','=',$contact->id)->get();
        return Response::json($contact[0]);

        } else {
        return "Not Authorized User!!!";
        }
    }

    public function store(Request $request) {
        //dd($request);
     
        $contact = new Contact;
        $contact->user_id      =   Auth::user()->id;
        $contact->name         =   $request->input('name');
        $contact->surname      =   $request->input('surname');        
        $contact->email        =   $request->input('email');
        $contact->phone        =   $request->input('phone');
        $contact->save();
        
        return $contact;
    }

    public function update(Request $request, Contact $contact)
    {
        $contact = Contact::where('id','=',$contact->id)->first();
        $contact->name         =   $request->input('name');
        $contact->surname      =   $request->input('surname');        
        $contact->email        =   $request->input('email');
        $contact->phone        =   $request->input('phone');
        $contact->save();

        return $contact;

    }

    public function destroy(Contact $contact)
    {
        $contact = Contact::where('id','=',$contact->id)->first();
        $contact ->delete();
        
        return "Contact Deleted !!!";
    }
}
