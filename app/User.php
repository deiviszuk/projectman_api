<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    
    protected $fillable = [
        'name', 'email', 'password', 'is_verified'
    ];

    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function contacts(){
        return $this->hasMany('App\Contact');
    }

    public function notes(){
        return $this->hasMany('App\Note');
    }
    public function projects(){
        return $this->hasMany('App\Project');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

}
