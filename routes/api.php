<?php

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('recover', 'AuthController@recover');

Route::group(['middleware' => ['jwt.auth']], function() {

    Route::get('logout', 'AuthController@logout');
    Route::get('user', ['uses' => 'UserController@index']);

    Route::resources([
        'contacts'  => 'ContactController',
        'notes'     => 'NoteController',
        'projects'  => 'ProjectController'
    ]);

    Route::get('projects/{project}/all-tasks',             ['uses' => 'ProjectController@indexTasks']);
    Route::get('projects/{project}/show-task/{task}',      ['uses' => 'ProjectController@showTask']);
    Route::post('projects/{project}/add-task',             ['uses' => 'ProjectController@addTask']);
    Route::put('projects/{project}/update-task/{task}',    ['uses' => 'ProjectController@updateTask']);
    Route::put('projects/{project}/finish-task/{task}',    ['uses' => 'ProjectController@finishTask']);
    Route::delete('projects/{project}/delete-task/{task}', ['uses' => 'ProjectController@destroyTask']);

});

Route::get('/', function() {return 
"
| GET|HEAD  | /                                         |                  | Closure                                           
| GET|HEAD  | api/contacts                              | contacts.index   | App\Http\Controllers\ContactController@index      
| POST      | api/contacts                              | contacts.store   | App\Http\Controllers\ContactController@store      
| GET|HEAD  | api/contacts/create                       | contacts.create  | App\Http\Controllers\ContactController@create     
| GET|HEAD  | api/contacts/{contact}                    | contacts.show    | App\Http\Controllers\ContactController@show       
| PUT|PATCH | api/contacts/{contact}                    | contacts.update  | App\Http\Controllers\ContactController@update     
| DELETE    | api/contacts/{contact}                    | contacts.destroy | App\Http\Controllers\ContactController@destroy    
| GET|HEAD  | api/contacts/{contact}/edit               | contacts.edit    | App\Http\Controllers\ContactController@edit       
| POST      | api/login                                 |                  | App\Http\Controllers\AuthController@login         
| GET|HEAD  | api/logout                                |                  | App\Http\Controllers\AuthController@logout        
| GET|HEAD  | api/notes                                 | notes.index      | App\Http\Controllers\NoteController@index         
| POST      | api/notes                                 | notes.store      | App\Http\Controllers\NoteController@store         
| GET|HEAD  | api/notes/create                          | notes.create     | App\Http\Controllers\NoteController@create        
| DELETE    | api/notes/{note}                          | notes.destroy    | App\Http\Controllers\NoteController@destroy       
| PUT|PATCH | api/notes/{note}                          | notes.update     | App\Http\Controllers\NoteController@update        
| GET|HEAD  | api/notes/{note}                          | notes.show       | App\Http\Controllers\NoteController@show          
| GET|HEAD  | api/notes/{note}/edit                     | notes.edit       | App\Http\Controllers\NoteController@edit          
| GET|HEAD  | api/projects                              | projects.index   | App\Http\Controllers\ProjectController@index      
| POST      | api/projects                              | projects.store   | App\Http\Controllers\ProjectController@store      
| GET|HEAD  | api/projects/create                       | projects.create  | App\Http\Controllers\ProjectController@create     
| GET|HEAD  | api/projects/{project}                    | projects.show    | App\Http\Controllers\ProjectController@show       
| PUT|PATCH | api/projects/{project}                    | projects.update  | App\Http\Controllers\ProjectController@update     
| DELETE    | api/projects/{project}                    | projects.destroy | App\Http\Controllers\ProjectController@destroy    
| POST      | api/projects/{project}/add-task           |                  | App\Http\Controllers\ProjectController@addTask    
| GET|HEAD  | api/projects/{project}/all-tasks          |                  | App\Http\Controllers\ProjectController@indexTasks 
| DELETE    | api/projects/{project}/delete-task/{task} |                  | App\Http\Controllers\ProjectController@destroyTask
| GET|HEAD  | api/projects/{project}/edit               | projects.edit    | App\Http\Controllers\ProjectController@edit       
| GET|HEAD  | api/projects/{project}/show-task/{task}   |                  | App\Http\Controllers\ProjectController@showTask   
| PUT       | api/projects/{project}/update-task/{task} |                  | App\Http\Controllers\ProjectController@updateTask 
| POST      | api/recover                               |                  | App\Http\Controllers\AuthController@recover       
| POST      | api/register                              |                  | App\Http\Controllers\AuthController@register      
| GET|HEAD  | api/user                                  |                  | App\Http\Controllers\UserController@index         ";
}

);

