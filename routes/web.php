<?php

use App\User;
use App\Http\Resources\UserResource;

Route::get('/', function () {
    return view('welcome');
});
///////////// API //////////////////////

// Registracija
// Route::post("/api/register", "UserController@store");

// Route::get('/api/logout', function() {
//     return Redirect::to(preg_replace("/:\/\//", "://log-me-out:fake-pwd@", url('api/logout')));
// });

// ///// Su Basic Auth ////////////////////
// //Autentifikacija
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('api/user', ['uses' => 'UserController@index','middleware'=>'simpleauth']);


//// Contact /////////////////////////////

//Route::post('api/contact', ['uses' => 'ContactController@store','middleware'=>'simpleauth']);
